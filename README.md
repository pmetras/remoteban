# Remote Ban

Block access attempts to a port-forwarded server at the Openwrt firewall.

## How it works

Fail2ban is installed on the server running multiple services that we want to protect. This server, in the LAN or a DMZ, is visible from the Internet through ports forwarded from the Openwrt router. When fail2ban detects an anomaly, it calls a CGI script on the router that adds an alert message in the router system log. The service of the banIP application, on the Openwrt router, scans the system log for such messages and blocks the attacking IP address at the firewall.

References: [banIP support thread](https://forum.openwrt.org/t/banip-support-thread/16985/1661)

Why doing it that way instead of centralizing the logs onto the router, like suggested by the banIP author?

- There is less traffic from the server to the router: instead of moving all the log lines, only the remoteBan action events are sent to the router.
- Fail2ban manages automatically the systemd Journal, without needing to create log files and using a rsyslog daemon. And you have log files, you have to manage log rotation and cleaning...
- You close the access at the door, locking all ports, denying any access from that IP address.

Another possibility would have been to install and configure a firewall on the server, let the traffic go through and block attacks on the server with Fail2Ban. This allows more precise bans for specific services and ports, with support for unban. But I consider that when you are attacked from the Internet, you don't need to discriminate the service or the port and you must totally block the attacker. And this must be done at the gateway level.

The main disadvantage is that the Fail2Ban documentation is not really user-friendly.

## Security

If the LuCI web interface of the router is open to the Internet, don't use the `remoteBan` CGI script! Use instead the alternate method that communicate to the router with sshd (look at `ssh_remoteban` action in the `banip.conf` file). If the CGI script is open on the Internet, anyone could block some IP addresses from accessing your server. The CGI script `remoteBan` must only be used on a secured LAN as it does not need authentication to execute commands.

## Requirements

### On the Openwrt router

- banIP: https://github.com/openwrt/packages/blob/master/net/banip/files/README.md
- remoteBan CGI script: https://gitlab.com/pmetras/remoteban

### On the server

- fail2ban: https://github.com/fail2ban/fail2ban

## Configuration

### On the Openwrt router

I assume that you have already configured banIP to your needs, selecting the range of IP that you want to block, and that banIP is running correctly.

1. In the menu Services > banIP, tab Log Settings, add a new Log Term "remoteBan Ban IP" to the list. The banIP service will scan for this string into the system log and will trigger the IP ban when encountered.
2. If your log is very verbose, probably because you have a lot of traffic, you can increase the Log Limit number to more than 100 (default).
3. [Save & Apply] your changes. That's it for banIP.

The corresponding uci options, in the `/etc/default/banip` file, in case you prefer to use the command line instead of the LuCI web interface.

```
config banip 'global'
	... your options ...
	option ban_loglimit '250'  
	list ban_logterm 'remoteBan Ban IP'
```

4. Security measure: you router must use SSL for the LuCI web interface! If that's not the case:

    1. Install the `luci-ssl` package.
    2. Enable HTTPS support in the menu System > Administration, tab HTTP(S) Access.
    3. [Save & Apply] your changes.

5. Unfortunately for web users, the following can't be done with the web interface.

```sh
$ ssh root@openwrt.example.com
root@openwrt.example.com's password: 


BusyBox v1.36.1 (2023-10-09 21:45:35 UTC) built-in shell (ash)

  _______                     ________        __
 |       |.-----.-----.-----.|  |  |  |.----.|  |_
 |   -   ||  _  |  -__|     ||  |  |  ||   _||   _|
 |_______||   __|_____|__|__||________||__|  |____|
          |__| W I R E L E S S   F R E E D O M
 -----------------------------------------------------
 OpenWrt 23.05.0, r23497-6637af95aa
 -----------------------------------------------------
```

6. Goto into the `/etc/config` directory.

```sh
# cd /etc/config
```

7. Copy the `remoteBan` CGI script into that directory. This can be done with a `scp` copy from another host, or downloading from the Internet like shown:

```sh
# wget https://gitlab.com/pmetras/remoteban/-/raw/main/remoteBan

# ls -al remoteBan
-rwxr-xr-x    1 root     root          6594 Nov 29 17:41 remoteBan
```

We do it that way so the remoteBan file will be included into the router backup, saving you the need to replay that procedure in case of Openwrt upgrade.

8. Make it executable.

```sh
# chmod a+x remoteBan
```

9. Create a symlink to that file in the `/www/cgi-bin`.

```sh
# ln -s remoteBan /www/cgi-bin/remoteBan
```

10. We are going to check that the CGI script is operational... Execute the command on the first line. The execution should print the following lines.

```sh
# QUERY_STRING="a=S&s=sshd" /www/cgi-bin/remoteBan
Content-type: Text/html

<html><head><title>remoteBan Logger</title></head><body>
<h2>Add an entry into syslog from fail2ban with IP address from query string</h2>
<p>QUERY_STRING : 'a=S&s=sshd'</p>
<p>unescape(QUERY_STRING) : 'a=S&s=sshd'</p>
<p>logmsg : 'remoteBan Start ban for service=sshd'</p>
</body>
```

11. Check that remoteBan action has appeared into the system log. Here again, execute only the first line and look from remoteBan signature.

```sh
# logread | tail
...
Wed Nov 29 20:22:14 2023 user.alert remoteBan[485]: remoteBan Start ban for service=sshd
```

You can also check that the message appears in the system log from LuCI: menu Status > System Log.

12. Everything is setup on the Openwrt router site. You can end the ssh session.

```sh
# exit
```

### On the server

The following instructions are for a Debian server, using systemd for logging. You must adapt them to your distribution and setup.

1. Fail2ban has been installed with the packages manager. It should be operational...

```sh
$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
     Loaded: loaded (/lib/systemd/system/fail2ban.service; enabled; preset: enabled)
     Active: active (running) since Mon 2023-11-27 22:41:16 EST; 1 day 21h ago
       Docs: man:fail2ban(1)
    Process: 166397 ExecReload=/usr/bin/fail2ban-client reload (code=exited, status=0/SUCCESS)
   Main PID: 3919 (fail2ban-server)
      Tasks: 17 (limit: 18815)
     Memory: 54.7M
        CPU: 3min 25.270s
     CGroup: /system.slice/fail2ban.service
             └─3919 /usr/bin/python3 /usr/bin/fail2ban-server -xf start

Nov 29 18:18:39 katalina fail2ban[3919]: Jail 'sshd' reloaded
Nov 29 18:18:39 katalina fail2ban[3919]: Jail 'apache-auth' reloaded
```

2. Copy the `banip.conf` action file into `/etc/fail2ban/action.d` directory.

This file contains also another way to control banIP on the router using ssh instead of the `remoteBan` CGI script. You can use it instead, sharing a ssh key between the server and the router for password-less authentification. The main disadvantage of that protocol way is that multiple lines are written to the router system log (ssh session opened; remoteBan; ssh session disconnected) instead of a unique remoteBan line. Look at the `ssh_remoteban` init tag for the command.

3. Create of edit the `/etc/fail2ban/jail.local` file to define which services to monitor and trigger the `banip` action in case of attack. The following is a partial example that you must adapt to your configuration.

```sh
$ cat /etc/fail2ban/jail.local
...
[DEFAULT]
# "bantime" is the number of seconds that a host is banned.
# In fact, it is permanently banned as banIP cannot unban a host. We put a long ban duration...
bantime  = 30d

# Default banning action (e.g. iptables, iptables-new,
# iptables-multiport, shorewall, etc) It is used to define
# action_* variables. Can be overridden globally or per
# section within jail.local file
#
# On this server, the default action is to block acces from the router
# firewall, using banIP and the remoteBan CGI script.
banaction = banip
banaction_allports = banip

[sshd]

# To use more aggressive sshd modes set filter parameter "mode" in jail.local:
# normal (default), ddos, extra or aggressive (combines all).
# See "tests/files/logs/sshd" or "filter.d/sshd.conf" for usage example and details.
#mode   = normal
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s
enabled = true

[apache-auth]

port     = http,https
logpath  = %(apache_error_log)s
enabled = true

...
```

## Configuration

When everything is setup, you only need to create the `action.d/banip.local` file and replace the value `openwrt.example.com` by the name or IP address of your router.

For instance, if your router has LAN IP address 192.168.1.1, the `banip.local` file contains:

```ini
[Init]
# The URL of the CGI script
bancgi = https://192.168.1.1/cgi-bin/remoteBan

# Alternative call to the remote ban
ssh_remoteban = /usr/bin/ssh root@192.168.1.1 /usr/bin/logger -p user.alert -t remoteBan

# Disable email notification
emailnotify = false
```

You can also disable email notification everytime a Fail2Ban action occurs, like shown in the example above.

### [NOT TESTED]

Another simpler configuration would be to specify the parameters from the `jail.local` file. One advantage of this way of doing it is that you can use different parameters, for instance to get email notification for some services and not for others.

For instance, the example `jail.local` above could contain the following lines to define the Openwrt IP address and to disable sending an email when `apache-auth` bans an IP:

```ini
...
[sshd]

# To use more aggressive sshd modes set filter parameter "mode" in jail.local:
# normal (default), ddos, extra or aggressive (combines all).
# See "tests/files/logs/sshd" or "filter.d/sshd.conf" for usage example and details.
#mode   = normal
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s
banaction = banip[routername=192.168.1.1]
enabled = true

[apache-auth]

port     = http,https
logpath  = %(apache_error_log)s
banaction = banip[routername=192.168.1.1, emailnotification=false]
enabled = true
...
```

## Testing the whole architecture.

Now you have to wait for the next attack on your server. If you've kept the email notification in the `banip.conf` actions, you should receive an email when it occurs. Then you can check the system log of the Openwrt router. You must see that Fail2ban has added lines with the offending IP and than banIP has caught them, and added them to the firewall rules.

Something like:

```
Sun Dec  3 14:30:41 2023 user.alert remoteban[31250]: remoteBan Ban IP=192.241.239.46 user=*UNKNOWN* service=sshd
Sun Dec  3 14:30:56 2023 user.alert remoteban[31257]: remoteBan Ban IP=94.102.61.23 user=*UNKNOWN* service=sshd
Sun Dec  3 14:30:57 2023 user.info banIP-0.9.2-4[24986]: suspicious IP '192.241.239.46'
Sun Dec  3 14:31:13 2023 user.info banIP-0.9.2-4[24986]: add IP '192.241.239.46' (expiry: -) to blocklistv4 set
Sun Dec  3 14:31:13 2023 user.info banIP-0.9.2-4[24986]: add IP '192.241.239.46' to local blocklist
Sun Dec  3 14:31:29 2023 user.info banIP-0.9.2-4[24986]: suspicious IP '94.102.61.23'
Sun Dec  3 14:31:45 2023 user.info banIP-0.9.2-4[24986]: add IP '94.102.61.23' (expiry: -) to blocklistv4 set
Sun Dec  3 14:31:45 2023 user.info banIP-0.9.2-4[24986]: add IP '94.102.61.23' to local blocklist
```

### When it does not work as expected

Here are a few places to look at:

1. Check in the server Fail2Ban log that it is running correctly: `journalctl -u fail2ban.service`.
2. Check Fail2Ban configuration: `fail2ban-client -dvv`
3. The communication with the router is working: `wget "https://openwrt.example.com/remoteBan?a=S&s=test"
4. Check in the router system log that banIP is catching the IP: `wget "https://openwrt.example.com/remoteBan?a=B&i=94.102.61.23"
5. Check that the IP has been added to the blocked IP on the router: `cat /etc/banip/banip.blocklist`
6. On the router, search for the IP in banIP set: `/etc/init.d/banip search 94.102.61.23`
7. Read [Fail2Ban wiki](https://github.com/fail2ban/fail2ban/wiki)

## License

This code is released under the MIT license.

## Authors

pierre@alterna.tv <Pierre Métras> - 2023

